import numpy as np
import torch
import matplotlib.pyplot as plt

from torchvision import datasets
import torchvision.transforms as transforms

# Define data loader parameters
num_workers = 0
batch_size = 64

#data set and data transform preprocessing
transform = transforms.ToTensor()
train_data = datasets.MNIST(root ='data', train=True, download=True, transform=transform)

#load data to a data loader
train_loader = torch.utils.data.DataLoader(train_data, batch_size=batch_size, num_workers=num_workers)


# take one itereation
dataiter = iter(train_loader)
images, labels = dataiter.next()
images = images.numpy()

# plot to show one image from dataset
img = np.squeeze(images[0])

fig = plt.figure(figsize= (3,3))
ax = fig.add_subplot(111)
ax.imshow(img, cmap='gray')

# plt.show()  #  uncomment to show image at this point

########### DISCRIMINATOR MODEL #################

import torch.nn as nn
import torch.nn.functional as F

class Discriminator(nn.Module):
    def __init__(self, input_size, hidden_dim, output_size):
        super(Discriminator, self).__init__()

        self.fc1 = nn.Linear(input_size, hidden_dim*4)
        self.fc2 = nn.Linear(hidden_dim*4, hidden_dim*2)
        self.fc3 = nn.Linear(hidden_dim*2, hidden_dim)
        self.fc4 = nn.Linear(hidden_dim, output_size)

        self.dropout = nn.Dropout(0.3)


    def forward(self, x):

        x = x.view(-1,28*28)

        x = F.leaky_relu(self.fc1(x),0.2) # (input, negative_slope =0.2)

        x = F.leaky_relu(self.fc2(x),0.2) # (input, negative_slope =0.2)
        x = self.dropout(x)
        x = F.leaky_relu(self.fc3(x),0.2) # (input, negative_slope =0.2)
        x = self.dropout(x)
        x = self.fc4(x) # (input, negative_slope =0.2)

        return x

########### Generator MODEL #################


class Generator(nn.Module):
    def __init__(self, input_size, hidden_dim, output_size):
        super(Generator, self).__init__()

        self.fc1 = nn.Linear(input_size, hidden_dim)
        self.fc2 = nn.Linear(hidden_dim, hidden_dim*2)
        self.fc3 = nn.Linear(hidden_dim*2, hidden_dim*4)
        self.fc4 = nn.Linear(hidden_dim*4, output_size)

        self.dropout = nn.Dropout(0.3)


    def forward(self, x):

        x = F.leaky_relu(self.fc1(x),0.2) # (input, negative_slope =0.2)
        x = self.dropout(x)
        x = F.leaky_relu(self.fc2(x),0.2) # (input, negative_slope =0.2)
        x = self.dropout(x)
        x = F.leaky_relu(self.fc3(x),0.2) # (input, negative_slope =0.2)
        x = self.dropout(x)
        x = F.tanh(self.fc4(x))

        return x




## Model Hyper parameters

# Discriminator Hyper params
input_size = 784  # size of input image to discriminator (28*28)
d_output_size = 1 # suze of discriminator output (real or fake)
d_hidden_size = 32 # size of last hidden layer in discriminator

# Generator Hyper parameters
z_size = 100 # size of latent vector to give Generator
g_output_size = 784 # size of discriminator output (generated image)
g_hidden_size = 32 # Size of first hidden layer in the Generator



## instantiating the models

D = Discriminator(input_size, d_hidden_size, d_output_size)
G = Generator(z_size, g_hidden_size, g_output_size)

print(D)
print(G)

## Defining the Loss functions of the generator and the Discriminator

## Loss functions for real and fake
def real_loss(D_out, smooth=False):
    batch_size = D_out.size()
    if smooth:
        labels = torch.ones(batch_size)*0.9
    else:
        labels = torch.ones(batch_size)

    criterion = nn.BCEWithLogitsLoss()

    loss = criterion(D_out.squeeze(), labels)

    return loss

def fake_loss(D_out):
    batch_size = D_out.size(0)
    labels = torch.zeros(batch_size)

    criterion = nn.BCEWithLogitsLoss()
    loss = criterion(D_out.squeeze(), labels)

    return loss

# Optimizers
import torch.optim as optim


lr = 0.002

d_optimizer = optim.Adam(D.parameters(), lr)
g_optimizer = optim.Adam(G.parameters(), lr)



#################### Training ###########################

import pickle as pkl

# model variables
num_epochs = 100

samples = []
losses = []

print_every = 400

sample_size = 16
fixed_z = np.random.uniform(-1, 1, size = (sample_size, z_size))
fixed_z = torch.from_numpy(fixed_z).float()

# train the network

D.train()
G.train()

for epoch in range(num_epochs):

    for batch_i, (real_images, _) in enumerate(train_loader):

        batch_size = real_images.size(0)

        real_images = real_images*2 - 1 # rescale input images from [0,1] to [-1,1]

        ###### Training the Discriminator ######

        d_optimizer .zero_grad()

        #1.  training with real images
        D_real = D(real_images)
        d_real_loss = real_loss(D_real.squeeze(), smooth = True) # computer the discriminator losses on real images, smooth the real labels


        #2. training with fake images

        # Generate Fake images
        z = np.random.uniform(-1,1, size=(batch_size, z_size))
        z= torch.from_numpy(z).float()
        fake_images = G(z)

        # calculate discriminator losses on fake images
        D_fake = D(fake_images)
        d_fake_loss = fake_loss(D_fake)


        #3. Sum of both losses and backpropagation step

        d_loss = d_real_loss + d_fake_loss
        d_loss.backward()

        d_optimizer.step()

        ########## training the generator ##########
        g_optimizer.zero_grad()

        #1. Train with fake images and flip the labels

        #generate the fake images
        z = np.random.uniform(-1, 1, size=(batch_size, z_size))
        z = torch.from_numpy(z).float()
        fake_images = G(z)
        #computer generator loss
        D_fake = D(fake_images) # compute the discriminator output of the fake images
        g_loss = real_loss(D_fake.squeeze()) # use real loss to flip labels to compute generator loss

        g_loss.backward()
        g_optimizer.step()

        #print some loss stats
        if batch_i % print_every == 0:
            print('Epoch [{:5d}/{:5d}] | d_loss: {:6.4f} | g_loss: {:6.4f}'.format(
                    epoch+1, num_epochs, d_loss.item(), g_loss.item()))
    ## After each Epoch ##
    # append discriminator loss and generator loss
    losses.append((d_loss.item(),g_loss.item()))

    #generate and save sample, fake_images
    G.eval()
    samples_z = G(fixed_z)
    samples.append(samples_z)
    G.train()

with open('train_samples.pkl', 'wb') as f:
    pkl.dump(samples, f)


################################################ training losses visualization ################################################################

fig, ax = plt.subplots()
losses = np.array(losses)
plt.plot(losses.T[0], label='Discriminator')
plt.plot(losses.T[1], label='Generator')
plt.title('Training Losses')
plt.legend()

## load samples generated from Training

# helper function for viewing a list of passed in sample images
def view_samples(epoch, samples):
    fig, axes = plt.subplots(figsize=(7,7), nrows=4, ncols=4, sharey=True, sharex=True)
    for ax, img in zip(axes.flatten(), samples[epoch]):
        img = img.detach()
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)
        im = ax.imshow(img.reshape((28,28)), cmap='Greys_r')


with open('train_samples.pkl','rb') as f:
    samples = pkl.load(f)

view_samples(-1, samples)


rows = 10 # split epochs into 10, so 100/10 = every 10 epochs
cols = 6
fig, axes = plt.subplots(figsize=(7,12), nrows=rows, ncols=cols, sharex=True, sharey=True)


# generated image every 10 epochs to show progression
for sample, ax_row in zip(samples[::int(len(samples)/rows)], axes):
    for img, ax in zip(sample[::int(len(sample)/cols)], ax_row):
        img = img.detach()
        ax.imshow(img.reshape((28,28)), cmap='Greys_r')
        ax.xaxis.set_visible(False)
        ax.yaxis.set_visible(False)


####### Sampling from the generator #######

# randomly generated, new latent vectors
sample_size=16
rand_z = np.random.uniform(-1, 1, size=(sample_size, z_size))
rand_z = torch.from_numpy(rand_z).float()

G.eval() # eval mode
# generated samples
rand_images = G(rand_z)

# 0 indicates the first set of samples in the passed in list
# and we only have one batch of samples, here
view_samples(0, [rand_images])

plt.show()
